// 1 ogromni (velicina 4)
// 2 velika (velicina 3)
// 2 srednja (velicina 2)
// 2 mala (velicina 1)

export const initializeBoard = () => {
  const row = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  let board = [];
  for (let i = 0; i < 10; i++) {
    board.push(row);
  }
  return randomizeShips(board);
};

const markForbidden = (board, coords) => {
  let begin = [],
    end = [];
  begin.push(coords[0][0] - 1);
  begin.push(coords[0][1] - 1);
  end.push(coords[coords.length - 1][0] + 1);
  end.push(coords[coords.length - 1][1] + 1);
  for (let i = begin[0]; i <= end[0]; i++) {
    for (let j = begin[1]; j <= end[1]; j++) {
      if (
        i < 0 ||
        i > 9 ||
        j < 0 ||
        j > 9 ||
        coords.find((el) => el[0] === i && el[1] === j)
      ) {
        continue;
      }
      board[i][j] = 4;
    }
  }
};

const makeShip = (board, size) => {
  const horizontal = Math.random() < 0.5;
  let coordinates = [
    Math.floor(Math.random() * (10 - size + 1)),
    Math.floor(Math.random() * (10 - size + 1)),
  ];
  let potentialCoords = [];
  for (let i = 0; i < size; i++) {
    if (
      !board[horizontal ? coordinates[0] : coordinates[0] + i][
        horizontal ? coordinates[1] + i : coordinates[1]
      ] ||
      board[horizontal ? coordinates[0] : coordinates[0] + i][
        horizontal ? coordinates[1] + i : coordinates[1]
      ] === 4
    ) {
      return makeShip(board, size);
    } else {
      potentialCoords.push(
        horizontal
          ? [coordinates[0], coordinates[1] + i]
          : [coordinates[0] + i, coordinates[1]]
      );
    }
  }
  potentialCoords.map(
    (coordinate) => (board[coordinate[0]][coordinate[1]] = 0)
  );
  markForbidden(board, potentialCoords);
  return board;
};

export const randomizeShips = (board) => {
  let fullBoard = JSON.parse(JSON.stringify(board));
  fullBoard = makeShip(fullBoard, 4);
  fullBoard = makeShip(fullBoard, 3);
  fullBoard = makeShip(fullBoard, 3);
  fullBoard = makeShip(fullBoard, 2);
  fullBoard = makeShip(fullBoard, 2);
  fullBoard = makeShip(fullBoard, 2);
  fullBoard = makeShip(fullBoard, 1);
  fullBoard = makeShip(fullBoard, 1);
  fullBoard = makeShip(fullBoard, 1);
  fullBoard = makeShip(fullBoard, 1);
  return fullBoard;
};
