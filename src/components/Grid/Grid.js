import React from "react";
import styled from "styled-components";
import GridField from "../../containers/GridField";
import { initializeBoard } from "./utils";
import { Button } from "antd";

const Wrapper = styled.div`
  padding: 0;
  margin: 20px ${(props) => (props.enemy ? "20%" : "20px")} 20px
    ${(props) => (!props.enemy ? "20%" : "20px")};
  display: block;
  float: ${(props) => (props.enemy ? "right" : "left")};
  filter: ${(props) =>
    !props.enemy || (props.enemy && props.turn && props.started)
      ? "blur(0px)"
      : "blur(4px)"};
`;

const Row = styled.div`
  margin: 0 0 0 0;
  box-sizing: border-box;
  height: 50px;
`;

const Grid = (props) => {
  // const [board, setBoard] = useState(initializeBoard());

  const initializeGrid = (matrix) => {
    // console.log(matrix);
    return matrix.map((row, i) => (
      <Row key={Math.random()}>
        {row.map((element, j) => (
          <GridField
            enemy={props.enemy}
            status={element}
            key={Math.random()}
            onClick={() =>
              props.enemy && props.started && props.turn
                ? onClickHandler(i, j)
                : null
            }
          />
        ))}
      </Row>
    ));
  };

  // ON READY SOCKET LISTENER!

  const onClickHandler = (i, j) => {
    const state = props.board[i][j];
    if (state === 1 || state === 4) {
      // miss
      let newBoard = JSON.parse(JSON.stringify(props.board));
      newBoard[i][j] = 2;
      props.changeBoard(newBoard);
      props.handleMove(newBoard, false);
      props.changeTurn()
      return 2;
    } else if (state === 0) {
      // hit
      let newBoard = JSON.parse(JSON.stringify(props.board));
      newBoard[i][j] = 3;
      props.changeBoard(newBoard);
      props.handleMove(newBoard, true);
      props.changeTurn()
      return 3;
    }
    return null;
  };

  const grid = initializeGrid(props.board);
  console.log(props);
  return (
    <Wrapper enemy={props.enemy} started={props.started} turn={props.turn}>
      <p>{props.enemy ? "Opponent" : "You"}</p>
      {grid}
      {!props.enemy && !props.started ? (
        <>
          <p>
            Don't like the layout?
            <Button
              type="link"
              onClick={() => {
                const newBoard = initializeBoard();
                props.changeBoard(newBoard);
              }}
            >
              Randomize
            </Button>
          </p>
          <Button
            type="primary"
            onClick={() => {
              props.playerReady();
            }}
          >
            Ready
          </Button>
        </>
      ) : null}
    </Wrapper>
  );
};

export default Grid;

// OZNAKE NA POLJIMA:
// 0 -> NEOZNACENO, ZAUZETO POLJE
// 1 -> NEOZNACENO, PRAZNO POLJE
// 2 -> OZNACENO, PRAZNO POLJE
// 3 -> OZNACENO, ZAUZETO POLJE (POGOĐENO)
// 4 -> NEOZNACENO, REZERVISANO POLJE
