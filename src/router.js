import React from "react";
import { Switch, Route } from "react-router-dom";
import MainMenu from "./containers/MainMenu/MainMenu";
import Game from './containers/Game/Game';

const Routes = () => (
  <Switch>
    <Route path="/" exact component={MainMenu} />
    <Route path="/play/:gameId" exact component={Game} />
  </Switch>
);

export default Routes;
