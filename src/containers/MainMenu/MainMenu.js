import React from "react";
import MainOption from "./MainOption";
import styled from "styled-components";

const Wrapper = styled.div`
  margin: 100px auto 0;
  padding: 30px 20px;
  width: 25%;
  border: 5px #c70720 solid;
  border-radius: 8px;
  box-sizing: border-box;
`;

const getRandomGameId = () => {
  return Math.random().toString(16).substr(2, 5);
};

const MainMenu = (props) => {

  return (
    <Wrapper>
      <MainOption
        label="Play"
        onClick={() =>
          props.history.push({ pathname: `play/${getRandomGameId()}` })
        }
      />
      <MainOption label="How to play" />
      <MainOption label="About" />
    </Wrapper>
  );
};

export default MainMenu;
