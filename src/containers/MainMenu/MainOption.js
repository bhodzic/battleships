import React from 'react';
import styled from 'styled-components';

const Button = styled.div`
    margin: 20px auto;
    padding: 10px 0;
    box-sizing: border-box;
    border-radius: 5px;
    border: 2px #C70720 solid;
    text-align: center;

    &:hover {
        border-color: #7e0716;
        background-color: #C0FDFB;
        cursor: pointer;
        font-size: 110%;
    }
`;

const MainOption = props => {

    return (
        <Button onClick={() => props.onClick()}>{props.label}</Button>
    )
}

export default MainOption;