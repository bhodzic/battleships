import React, { useState, useEffect } from "react";
import { Typography } from "antd";
import { Prompt } from "react-router-dom";
import styled from "styled-components";
import Grid from "../../components/Grid/Grid";
import socketIOClient from "socket.io-client";
import { initializeBoard } from "../../components/Grid/utils";
const ENDPOINT = "https://battleship-socket.herokuapp.com:4040";

const { Text } = Typography;

const Wrapper = styled.div`
  padding: 100px 50px;
  display: inline;
`;

const Game = (props) => {
  const [id] = useState(props.location.pathname.split(/\//g)[2]);
  const [socket] = useState(socketIOClient(ENDPOINT));
  const [board1, setBoard1] = useState(initializeBoard());
  const [board2, setBoard2] = useState(initializeBoard());
  const [first, setFirst] = useState(true);
  const [started, setStarted] = useState(false);
  const [turn, setTurn] = useState(true);

  const changeBoard1 = (newBoard) => {
    setBoard1(newBoard);
  };

  const changeBoard2 = (newBoard) => {
    setBoard2(newBoard);
  };

  const player1Ready = () => {
    socket.emit("player1Ready", { gameId: id, board: board1 });
  };

  const player2Ready = () => {
    socket.emit("player2Ready", { gameId: id, board: board2 });
  };

  const changeTurn = () => setTurn(!turn);

  const handleMove = (newBoard, hit) => {
    socket.emit("move", { gameId: id, board: newBoard, hit: hit });
  };

  socket.on("updateField", (data) => {
    console.log(data);
    if (turn) {
      // potez je odigrao prvi igrac
      setBoard1(data.board);
    } else {
      setBoard2(data.board);
    }
    setTurn(!turn);
  });

  socket.on("player1Wins", (data) => {
    if (id === data.gameId) {
      alert(data.message);
    }
  });

  socket.on("player2Wins", (data) => {
    if (id === data.gameId) {
      alert(data.message);
    }
  });

  useEffect(() => {
    socket.emit("queued", { gameId: id, board1: board1, board2: board2 });

    socket.on("player1Connected", (data) => {
      setFirst(true);
      setTurn(true);
    });

    socket.on("player2Connected", (data) => {
      setFirst(false);
      setTurn(false);
    });

    socket.on("roomFull", (data) => {
      console.log("Room with id:" + data.gameId + " full");
    });

    socket.on("startGame", (data) => {
      if (data.gameId === id) {
        setBoard1(data.game.board1);
        setBoard2(data.game.board2);
        setStarted(true);
      }
    });

    socket.on("player1Move", (data) => {
      if (data.gameId === id) {
        if (first) {
          setTurn(true);
          setBoard2(data.game.board2);
        } else {
          setTurn(false);
          setBoard1(data.game.board1);
        }
        console.log(data, "noviObj");
      }
    });

    socket.on("player2Move", (data) => {
      if (data.gameId === id) {
        if (first) {
          setTurn(true);
          setBoard1(data.game.board1);
        } else {
          setTurn(false);
          setBoard2(data.game.board2);
        }
      }
    });

    return () => {
      socket.emit(first ? "Player1Disconnect" : "Player2Disconnect", {
        gameId: id,
      });
      socket.disconnect();
    };
  }, []);

  return (
    <>
      <div>
        <p>Send this link to you friend to begin the game:</p>
        <Text copyable>{`localhost:3000/play/${id}`}</Text>
      </div>
      <Wrapper>
        <Prompt when={true} message="Are you sure you want to leave?" />
        <Grid
          id={id}
          handleMove={handleMove}
          turn={turn}
          changeTurn={changeTurn}
          started={started}
          ready={started}
          enemy={true}
          socket={socket}
          board={first ? board2 : board1}
          changeBoard={first ? changeBoard2 : changeBoard1}
        />
        <Grid
          id={id}
          handleMove={handleMove}
          turn={turn}
          changeTurn={changeTurn}
          started={started}
          ready={true}
          enemy={false}
          socket={socket}
          board={first ? board1 : board2}
          changeBoard={first ? changeBoard1 : changeBoard2}
          playerReady={first ? player1Ready : player2Ready}
        />
      </Wrapper>
    </>
  );
};

export default Game;
