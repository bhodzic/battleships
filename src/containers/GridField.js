import React, { useState } from "react";
import styled from "styled-components";

const Field = styled.span`
  width: 50px;
  box-sizing: border-box;
  height: 50px;
  margin: 0;
  border: 1px black solid;
  display: inline-block;
  background-color: ${(props) =>
    props.status === 1 || props.status === 4
      ? "#fff"
      : props.status === 2
      ? props.enemy ? "#5cb1f7" : "#454545"
      : props.status === 3
      ? "#db0404"
      : !props.enemy ? "#5cb1f7" : "#fff"};

  &:hover {
    opacity: ${(props) => (props.enemy ? "0.5" : "1")};
    background-color: ${props => props.enemy ? "#9d9d9d" : 'null'};
  }
`;

const GridField = (props) => {
  const [status, setStatus] = useState(props.status);
  return (
    <Field
      enemy={props.enemy}
      status={status}
      onClick={() => {
        const newStatus = props.onClick();
        setStatus(newStatus || status);
      }}
    />
  );
};

export default GridField;
